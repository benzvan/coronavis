package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"io/ioutil"
	"os"
)

var version = "1.0.0-DEV"

func main() {
	log.SetFormatter(&log.JSONFormatter{})
	app := cli.NewApp()
	app.Name = "hop2prom"
	app.Usage = "takes a johns hopkins csv file and creates an openmetrics file for prometheus"
	app.Action = run
	app.Version = version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "log-level",
			Usage:  "log level for logging logs",
			EnvVar: "GO_LOG_LEVEL",
			Value:  "INFO",
		},
		cli.StringFlag{
			Name:  "input-file",
			Usage: "johns hopkins csv file to process",
		},
		cli.StringFlag{
			Name:  "metric-name",
			Usage: "metric name for output file from this data",
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Fatal("Error running application")
	}
}

func run(c *cli.Context) error {
	in, err := ioutil.ReadFile(c.String("input-file"))
	if err != nil {
		return err
	}
	out := os.Stdout
	// pass in/out to parser
	err = ReadLines(c.String("metric-name"), string(in), out)
	if err != nil {
		return err
	}
	return nil
}
