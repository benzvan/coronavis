package main

import (
	"bytes"
	"io"
	"time"

	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadHeader(t *testing.T) {
	assert := assert.New(t)
	// given
	header := `UID,iso2,Combined_Key,1/22/20,1/23/20`
	expectedLabelNames := []string{
		"UID",
		"iso2",
		"Combined_Key",
	}
	expectedDates := []string{
		"1/22/20",
		"1/23/20",
	}

	// when
	names, dates, _ := ReadHeader(header)

	// then
	assert.Equal(expectedLabelNames, names)
	assert.Equal(expectedDates, dates)
}

func TestReadLine(t *testing.T) {
	assert := assert.New(t)
	// given
	fields := []string{
		"UID",
		"iso2",
		"iso3",
		"code3",
		"FIPS",
		"Admin2",
		"Province_State",
		"Country_Region",
		"Lat",
		"Long_",
		"Combined_Key",
	}
	dates := []string{
		"1/22/20",
		"1/23/20",
	}
	line := `84001001,US,USA,840,1001.0,Autauga,Alabama,US,32.53952745,-86.64408227,"Autauga, Alabama, US",99,0`
	expectedLabels := map[string]string{
		"UID":            "84001001",
		"iso2":           "US",
		"iso3":           "USA",
		"code3":          "840",
		"FIPS":           "1001.0",
		"Admin2":         "Autauga",
		"Province_State": "Alabama",
		"Country_Region": "US",
		"Lat":            "32.53952745",
		"Long_":          "-86.64408227",
		"Combined_Key":   "Autauga, Alabama, US",
	}
	expectedFirstValue := OpenMetric{
		metricName: "confirmed",
		labels:     expectedLabels,
		value:      99,
		timestamp:  time.Date(2020, 1, 22, 0, 0, 0, 0, time.UTC),
	}
	expectedSecondValue := OpenMetric{
		metricName: "confirmed",
		labels:     expectedLabels,
		value:      0,
		timestamp:  time.Date(2020, 1, 23, 0, 0, 0, 0, time.UTC),
	}

	// when
	result, _ := ReadLine("confirmed", line, fields, dates)

	// then
	assert.Equal(expectedFirstValue, result[0])
	assert.Equal(expectedSecondValue, result[1])
}

func TestReadLines(t *testing.T) {
	assert := assert.New(t)
	// given
	lines := `UID,iso2,Combined_Key,1/22/20,1/23/20
84001001,US,"Autauga, Alabama, US",99,0
`

	expected := `confirmed{Combined_Key="Autauga, Alabama, US",UID="84001001",iso2="US"} 99 1579651200000
confirmed{Combined_Key="Autauga, Alabama, US",UID="84001001",iso2="US"} 0 1579737600000
`

	var buff bytes.Buffer
	out := io.Writer(&buff)

	// when
	ReadLines("confirmed", lines, out)

	result := buff.String()

	// then
	assert.Equal(expected, result)

}
