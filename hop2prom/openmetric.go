package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"sort"
	"time"
)

type OpenMetric struct {
	metricName string
	labels     labels
	value      int
	timestamp  time.Time
}

type labels map[string]string

func NewMetric(name string, labels map[string]string, value int, date string) (metric OpenMetric, err error) {
	timestamp, err := dateToTime(date)
	if err != nil {
		return
	}
	metric = OpenMetric{
		metricName: name,
		labels:     labels,
		value:      value,
		timestamp:  timestamp,
	}
	return
}

func dateToTime(date string) (time.Time, error) {
	return time.Parse("1/2/06", date)
}

func (l labels) MarshalText() (text []byte, err error) {
	buffer := bytes.NewBufferString("{")
	length := len(l)
	count := 0
	keys := []string{}
	for key := range l {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _, key := range keys {
		jsonValue, err := json.Marshal(l[key])
		if err != nil {
			return nil, err
		}
		buffer.WriteString(fmt.Sprintf("%s=%s", key, string(jsonValue)))
		count++
		if count < length {
			buffer.WriteString(",")
		}
	}
	buffer.WriteString("}")
	return buffer.Bytes(), nil
}

func (m *OpenMetric) toString() string {
	labels, _ := m.labels.MarshalText()
	return fmt.Sprintf(
		"%s%s %v %v",
		m.metricName,
		labels,
		m.value,
		m.timestamp.Unix(),
	)
}
