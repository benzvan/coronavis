package main

import (
	"time"

	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDateToTime(t *testing.T) {
	assert := assert.New(t)
	// given
	date := "1/1/20"
	expectedTime := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)

	// when
	result, _ := dateToTime(date)

	// then
	assert.Equal(expectedTime, result)
}

func TestNewMetric(t *testing.T) {
	assert := assert.New(t)
	// given
	labels := map[string]string{
		"labelName": "labelValue",
	}
	expectedMetric := OpenMetric{
		metricName: "deaths",
		labels:     labels,
		value:      7,
		timestamp:  time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
	}
	// when
	result, _ := NewMetric("deaths", labels, 7, "1/1/20")

	// then
	assert.Equal(expectedMetric, result)
}

func TestToString(t *testing.T) {
	assert := assert.New(t)
	// given
	labels := map[string]string{
		"labelName": "labelValue",
		"label2":    "value2",
	}
	metric := OpenMetric{
		metricName: "deaths",
		labels:     labels,
		value:      7,
		timestamp:  time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
	}
	expectedMetric := `deaths{label2="value2",labelName="labelValue"} 7 1577836800`

	// when
	result := metric.toString()

	// then
	assert.Equal(expectedMetric, result)
}
