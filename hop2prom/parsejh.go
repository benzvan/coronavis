package main

import (
	"encoding/csv"
	"io"
	"strconv"
	"strings"
)

func ReadHeader(lineIn string) (fields []string, dates []string, err error) {
	r := csv.NewReader(strings.NewReader(lineIn))

	lines, err := r.ReadAll()
	if err != nil {
		return
	}

	for i, field := range lines[0] {
		if _, ok := dateToTime(field); ok == nil {
			fields = lines[0][:i]
			dates = lines[0][i:]
			break
		}
	}
	return
}

func ReadLine(name string, lineIn string, fields []string, dates []string) (metrics []OpenMetric, err error) {
	r := csv.NewReader(strings.NewReader(lineIn))

	lines, err := r.ReadAll()
	if err != nil {
		return
	}

	line := lines[0]
	labels := map[string]string{}
	for i, field := range fields {
		labels[field] = line[i]
	}
	line = line[len(fields):]

	for i, date := range dates {
		value, _ := strconv.Atoi(line[i])
		metric, err := NewMetric(name, labels, value, date)
		if err != nil {
			return []OpenMetric{}, err
		}
		metrics = append(metrics, metric)
	}
	return
}

func ReadLines(name string, linesIn string, out io.Writer) (err error) {
	linesIn = strings.TrimSpace(linesIn)
	lines := strings.Split(string(linesIn), "\n")
	var fields []string
	var dates []string
	fields, dates, err = ReadHeader(lines[0])
	if err != nil {
		return
	}
	for _, line := range lines[1:] {
		var metrics []OpenMetric
		metrics, err = ReadLine(name, line, fields, dates)
		if err != nil {
			return
		}
		for _, metric := range metrics {
			out.Write([]byte(metric.toString() + "\n"))
		}
	}

	return
}
