#!/usr/bin/env sh

outfile="/shared/metrics"

echo "reading input files"
echo "# TYPE covid_confirmed_cases counter" > "${outfile}"
echo "# HELP covid_confirmed_cases Number of confirmed cases" >> "${outfile}"
hop2prom --input-file /data/short.csv --metric-name covid_confirmed_cases >> "${outfile}" 
#hop2prom --input-file /data/time_series_covid19_confirmed_US.csv --metric-name confirmed >> "${outfile}"
#hop2prom --input-file /data/time_series_covid19_deaths_US.csv --metric-name deaths >> "${outfile}"
echo "# EOF" >> "${outfile}"

echo "done reading input files"
touch /shared/hop2prom-done