#!/usr/bin/env sh

sleep 5
while [ ! -f /shared/hop2prom-done ]
do 
echo "waiting for data file"
sleep 1
done

echo "loading data file"
rm /shared/hop2prom-done
mkdir -p /shared/prometheus/
promtool tsdb create-blocks-from openmetrics \
  /shared/metrics /shared/prometheus