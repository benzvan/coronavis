# coronavis

Prometheus/Grafana visualizations for Johns Hopkins SARS-CoV2 data.

## Description
Imports data from [Johns Hopkins repo](https://github.com/CSSEGISandData/COVID-19) into OpenMetrics format and imports into a local prometheus container for visualization in a local grafana container.

## Installation
Requires a docker runtime like Colima, docker, and docker-compose

To start:
```
docker-compose up --build
```

To stop:
```
docker-compose down -v
```

## Usage
To view and explore data in grafana, go to http://localhost:8080 (currently commented out for quick start during development)

To view and explore data in prometheus, go to http://localhost:9090

## Contributing
Contributions are welcome.

## Project Notes
OpenMetrics Format
```
metric_name{label="value"} metric_value timestamp
http_requests_total{method="post",code="400"}  3   1395066363
```

## License
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
